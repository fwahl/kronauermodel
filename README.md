# kronauermodel

A Python implementation of the Kronauer model which models circadian phase shift based on light exposure.

## Using the package
If you use this package or adapt the code, please cite:

[1] Forger, D.B., Jewett, M.E. and Kronauer, R.E., 1999. A simpler model of the human circadian pacemaker. Journal of biological rhythms, 14(6), pp.533-538.

## Further information about the model
More information can be found in the following academic research papers.

[1] Forger, D.B., Jewett, M.E. and Kronauer, R.E., 1999. A simpler model of the human circadian pacemaker. Journal of biological rhythms, 14(6), pp.533-538.

[2] Kronauer, R.E., Czeisler, C.A., Pilato, S.F., Moore-Ede, M.C. and Weitzman, E.D., 1982. Mathematical model of the human circadian system with two interacting oscillators. American Journal of Physiology-Regulatory, Integrative and Comparative Physiology, 242(1), pp.R3-R17.
