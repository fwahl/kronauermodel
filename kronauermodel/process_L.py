#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Florian Wahl"
__copyright__ = "Copyright 2017, Florian Wahl"
__version__ = "0.0.1"
__maintainer__ = "Florian Wahl"
__email__ = "florian.wahl@gmail.com"
__status__ = "Development"


class ProcessL:
    """Implements the L process of the Kronauer model

    More information can be found in [1]. All default parameters were chosen according to [1].

    [1] Forger, D.B., Jewett, M.E. and Kronauer, R.E., 1999. A simpler model of the human circadian pacemaker.
        Journal of biological rhythms, 14(6), pp.533-538.
    """

    def __init__(self, dt, alpha_0=0.16, I_0=9500.0, p=0.6, G=19.875, beta=0.013):
        """ Initialise Process L."""
        self.dt = dt

        self.alpha_0 = alpha_0
        self.I_0 = I_0
        self.p = p
        self.G = G
        self.beta = beta

        self.n = 0.5
        self.B_hat = 0

    def update(self, I):
        """Compute drive B hat from light intensity I."""
        alpha = self.alpha_0 * (I / self.I_0) ** self.p
        self.n = self.n + (60*self.dt * ((alpha * (1 - self.n)) - (self.beta * self.n)))
        self.B_hat = self.G * (1 - self.n) * alpha
        return self.B_hat

    def get_B_hat(self):
        """Get current drive B hat."""
        return self.B_hat
