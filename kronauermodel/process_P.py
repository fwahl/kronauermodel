#!/usr/bin/env python
# -*- coding: utf-8 -*-


__author__ = "Florian Wahl"
__copyright__ = "Copyright 2017, Florian Wahl"
__version__ = "0.0.1"
__maintainer__ = "Florian Wahl"
__email__ = "florian.wahl@gmail.com"
__status__ = "Development"


class ProcessP:
    """Implements process P

        More information can be found in [1]. All default parameters were chosen according to [1].

    [1] Forger, D.B., Jewett, M.E. and Kronauer, R.E., 1999. A simpler model of the human circadian pacemaker.
        Journal of biological rhythms, 14(6), pp.533-538.
    """

    def __init__(self, dt, mu=0.13, k=0.55, q=(1 / 3), tau_x=24.2):
        """Initialise Process P."""
        self.dt = dt  # dt in hours

        self.mu = mu
        self.k = k
        self.q = q
        self.tau_x = tau_x

        self.B = 0
        self.x = -1.0
        self.x_c = 0.

    def update(self, B_hat):
        """Update Kronauer model.

        Use drive B hat to compute drive B and update state variables x and x_c.

        :param B_hat: Drive on Process P.
        :return: state variable x, modulated drive B, state variable x_c.
        """
        from math import pi

        x = self.x
        x_c = self.x_c

        dx = (pi / 12) * (x_c + self.mu * (((1 / 3) * x) + ((4 / 3) * x ** 3) - ((256 / 105) * x ** 7)) + self.B)
        dx_c = (pi / 12) * ((self.q * self.B * x_c) - (((24 / (0.99729 * self.tau_x)) ** 2) + (self.k * self.B)) * x)
        self.x = x + (dx * self.dt)
        self.x_c = x_c + (dx_c * self.dt)

        self.B = (1 - (0.4 * self.x)) * (1 - (0.4 * self.x_c)) * B_hat

        return self.x, self.B, self.x_c
