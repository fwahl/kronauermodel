#!/usr/bin/env python
# -*- coding: utf-8 -*-


__author__ = "Florian Wahl"
__copyright__ = "Copyright 2017, Florian Wahl"
__version__ = "0.0.1"
__maintainer__ = "Florian Wahl"
__email__ = "florian.wahl@gmail.com"
__status__ = "Development"

