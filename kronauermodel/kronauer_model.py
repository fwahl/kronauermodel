#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Florian Wahl"
__copyright__ = "Copyright 2017, Florian Wahl"
__version__ = "0.0.1"
__maintainer__ = "Florian Wahl"
__email__ = "florian.wahl@gmail.com"
__status__ = "Development"


class KronauerModel:
    """Implements the Kronauer Model

        More information can be found in [1]. All default parameters were chosen according to [1].

    [1] Forger, D.B., Jewett, M.E. and Kronauer, R.E., 1999. A simpler model of the human circadian pacemaker.
        Journal of biological rhythms, 14(6), pp.533-538.
    """

    def __init__(self, dt):
        """ Initialise Kronauer model.

        :param dt: time step width to use in model in hours, e.g. (1/720) equals 5 second time steps.
        """
        from kronauer_model import process_L
        from kronauer_model import process_P

        self.process_L = process_L.ProcessL(dt)
        self.process_P = process_P.ProcessP(dt)

    def update(self, I):
        """ Perform modelling step.

        :param I: Light intensity in lux
        :return: State variable x, drives B and B hat, state variable x_c
        """
        B_hat = self.process_L.update(I)
        x, B, x_c = self.process_P.update(B_hat)
        return x, B, B_hat, x_c
